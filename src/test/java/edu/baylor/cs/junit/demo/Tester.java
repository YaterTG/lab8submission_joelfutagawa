package edu.baylor.cs.junit.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import edu.Timer;
import edu.TimerException;

public class Tester {
	
	@DisplayName("Test Timer pass")
	@Test
	void passingTest() throws TimerException {
		Assertions.assertTrue(Timer.timeMe(1000) >= 1000);
	}
	@DisplayName("Test Timer fail")
	@Test
	void failOverTest() throws TimerException {
		Assertions.assertThrows(TimerException.class, () -> {
			Timer.timeMe(-1);
		});
	}

	@DisplayName("Test Timer edgecase")
	@Test
	void failOverTestEdge() throws TimerException {
		Assertions.assertTrue(Timer.timeMe(0) >= 0);
	}
}

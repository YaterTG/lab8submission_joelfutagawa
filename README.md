1.  They are different because in logger.properties, the console logger has its logging level set to 'INFO', which causes it to only display certain information. The file logger on the other hand has its logging level set to 'ALL', which means that it will display all relevant information.
1.  In my version of the logger.log file, I have two duplicates of this line, one right after the other. These lines come from the system checking to see whether or not the second and third test cases have been disabled or not so it can decided whether or not to run through them.
1.  Assertions.assertThrows creates an expectation of a function to throw a specified type of exception. If the specified exception or one of its subtypes is not thrown, the assertion will fail. 1.
    1.  The serialVersionUID ensures that classes and objects do not change between sender and receiver. It is needed because without it, different java compilers would produce different UID's and programs would be inconsistent between them.
    2.  We had to override the constructor so we could give a more specific error message than what would be provided by the superclass.
    3.  We did not need to override the other methods because their functionality is perfectly suited for what we are doing with the TimerException class.

1.  The static block is called at the end of the failOverEdge test case and what it does is that it uses the 'logger.properties' file to set up the console and file loggers.
1.  The .md stands for "markdown". It is a unique method of writing formatted text via code-like methods. In bitbucket, if a project features a .md file, it will be displayed on the source page of the project.
1.  The test is failing because the failOverTest is throwing a nullPointerException rather than a TimerException, as it was asserted that it would. This is because the return value is initialized to null. In the instance in which the method is supposed to throw a TimerException, the return value is never set to anything, and so in the "finally" block of the function, when the return value is called for a arithmetic operation, the function ends right there and throws a NullPointerException since there has been an attempt to call on a nullified pointer.
1.  The issue is that the assertion is looking for a TimerException, and it does not get it or any subclasses of it.
1.  See uploaded images
1.  See uploaded images
1.  TimerException is a level 1 extension of the superclass Exception, meaning that it directly extends and inherits from the Exception class. NullPointerException is a level 2 extension of the Exception superclass. First, RuntimeException extends the Exception class, and then NullPointerException extends RuntimeException.

